// ==UserScript==
// @name           Delete All Traces
// @namespace      http://openstreetmap.org
// @description    delete all the traces on this page
// @include        http://www.openstreetmap.org/user/h4ck3rm1k3/traces/page/
// ==/UserScript==
var mySeen = new Array();


function startProcess(){

//      alert("hello world");
  console.log("hello");
   var v = new RegExp();
   v.compile("\/traces\/([0-9]+)$");

   var allHRefTags=document.getElementsByTagName("a");
   for (i=0; i<allHRefTags.length; i++) {
//      <a href="/user/h4ck3rm1k3/traces/854903"
	if (m = v.exec(allHRefTags[i].href))
        {
	    var trace_id=RegExp.$1;
//  console.log(m);
    	    console.log("looking " + trace_id);
	    
	    if (!mySeen[trace_id])
		{
		    GM_xmlhttpRequest({
			method: "POST",
			//<form method="post" action="/trace/858721/delete" class="button-to"><div><input value="Delete this trace" type="submit"></div>
			url: "/trace/" + RegExp.$1 + "/delete",
			onload: function(response) {
			    //alert(response.responseText);
			    console.log(response.responseText);
			}
		    });
		}
	    else
		{
		    console.log("already seen " + trace_id);
		}

	    mySeen[trace_id]++;		
//	    sleep(1);

        }
        else
       {
               //alert("skip" + allHRefTags[i].href);
        }

   }
}
startProcess();
